FROM ubuntu:zesty

# UPDATE & ADD MUSL, RUST & AWS-CLI TOOLS
ENV PATH="/root/.cargo/bin:$PATH"
RUN apt-get update                                                       && \
    apt-get install -y --no-install-recommends                              \
        curl ca-certificates file make musl-tools g++ clang cmake awscli && \
    update-ca-certificates                                               && \
    apt-get clean                                                        && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y                         && \
    rustup target add x86_64-unknown-linux-musl

# BUILD STATIC NANOMSG W/ MUSL
RUN V=1.0.0                                                                                            && \
    cd /usr/local/src                                                                                  && \
    curl https://github.com/nanomsg/nanomsg/archive/$V.tar.gz -sSLf|tar oxz                            && \
    cd nanomsg-$V                                                                                      && \
    perl -p -i -e 's/^option.*NN_STATIC_LIB.*$/option \(NN_STATIC_LIB \"static\" ON\)/' CMakeLists.txt && \
    export CC='musl-gcc -static'                                                                       && \
    ./configure                                                                                        && \
    make install                                                                                       && \
    cd /usr/local/src                                                                                  && \
    rm -rf $V.tar.gz nanomsg-$V
