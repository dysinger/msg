extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    let src_dir = PathBuf::from("src");
    let src_file = src_dir.join("nanomsg.h");

    let out_dir = env::var("OUT_DIR").expect("OUT_DIR not set");
    let out_path = PathBuf::from(&out_dir);
    let out_file = out_path.join("nanomsg.rs");

    bindgen::Builder::default()
        .no_unstable_rust()
        .header(src_file.as_path().to_str().unwrap())
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(out_file)
        .expect("Couldn't write bindings!");

    if env::var("TARGET").expect("NO TARGET?").contains("musl") {
        // static musl linking nanomsg (that we install in /usr/local)
        println!("cargo:rustc-link-search=native=/usr/local/lib");
        println!("cargo:rustc-link-lib=static=nanomsg");
    } else {
        // default dynamic linking
        println!("cargo:rustc-link-lib=nanomsg");
    }
}
